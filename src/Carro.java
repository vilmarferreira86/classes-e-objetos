public class Carro {

    public static final String VERMELHO = "vermelho";
    public static final String PRETO = "preta";
    private Integer quantidadePneus;
    private Integer quantidadeCalotas;
    private Integer quantidadeParafusosPeneu;
    private Integer quantidadePortas;
    private boolean arCondicionado;
    private String cor;
    private boolean farolDeMilhas;
    private Integer numeroChassi;
    private Integer anoFabricacao;
    private String combustivel;


    public Carro(Integer quantidadePneus, Integer quantidadePortas, Integer numeroChassi, Integer anoFabricacao,
                 String combustivel){
        this.quantidadePneus = quantidadePneus;
        this.quantidadeCalotas = quantidadePneus;
        this.quantidadeParafusosPeneu = quantidadePneus * 4;
        this.quantidadePortas = quantidadePortas;
        this.numeroChassi = numeroChassi;
        this.anoFabricacao = anoFabricacao;
        this.combustivel = combustivel;
        this.arCondicionado = true;
        this.farolDeMilhas = false;
        this.cor = "Azul Marinho";
    }

    public Integer getQuantidadePneus() {
        return quantidadePneus + 1;
    }

    public void setQuantidadePneus(Integer quantidadePneus) {
        setQuantidadeCalotas(quantidadePneus);
        this.quantidadePneus = quantidadePneus;
    }

    public Integer getQuantidadeCalotas() {
        setQuantidadeCalotas(getQuantidadePneus());
        return quantidadeCalotas;
    }

    public void setQuantidadeCalotas(Integer quantidadeCalotas) {
        this.quantidadeCalotas = quantidadeCalotas;
    }

    public Integer getQuantidadeParafusosPeneu() {
        return quantidadeParafusosPeneu;
    }

    public void setQuantidadeParafusosPeneu(Integer quantidadeParafusosPeneu) {
        this.quantidadeParafusosPeneu = quantidadeParafusosPeneu;
    }

    public Integer getQuantidadePortas() {
        return quantidadePortas + 1; // Contando com a porta do porta - malas
    }

    public void setQuantidadePortas(Integer quantidadePortas) {
        this.quantidadePortas = quantidadePortas;
    }

    public boolean isArCondicionado() {
        return arCondicionado;
    }

    public void setArCondicionado(boolean arCondicionado) {
        this.arCondicionado = arCondicionado;
    }

    public String getCor() {
        return cor;
    }

    public boolean isFarolDeMilhas() {
        return farolDeMilhas;
    }

    public void setFarolDeMilhas(boolean farolDeMilhas) {
        this.farolDeMilhas = farolDeMilhas;
    }

    public void setCor(String cor){
        System.out.println(cor);
    }

    public Integer getNumeroChassi() {
        return numeroChassi;
    }

    public void setNumeroChassi(Integer numeroChassi) {
        this.numeroChassi = numeroChassi;
    }

    public Integer getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(Integer anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

    public void imprimeValores(){
        System.out.println("Quantidade de Pneus: "+getQuantidadePneus());
        System.out.println("Quantidade de calotas: "+getQuantidadeCalotas());
        System.out.println("Quantidade  de parafusos: "+getQuantidadeParafusosPeneu());
        System.out.println("Quantidade de portas : "+getQuantidadePortas());
        if(isArCondicionado()==true){
            System.out.println("Possui ar condicionado");
        }else{
            System.out.println("Não possui ar condicionado");
        }
        if(isFarolDeMilhas()==true){
            System.out.println("Possui farol de milhas");
        }else{
            System.out.println("Não possui farol de milhas");
        }
        System.out.println("Cor opcional: "+cor);
        System.out.println("Numero do chassi: "+getNumeroChassi());
        System.out.println("Ano Fabricação: "+getAnoFabricacao());
        System.out.println("Combustível: "+getCombustivel());
    }
}
