package um;

public class Primeiro {
    private static Integer variavel = 1;
    private Integer escopoClasse = 1;
    public final static Integer CONSTANTE = 10;

    public static Integer metodoEstatico(){
        return variavel;
    }

    public void metodoPublico(){
        System.out.println(escopoClasse);
        escopoClasse = 2;
        System.out.println(escopoClasse);
    }

    public void alteraValorVariavel(){
        System.out.println(escopoClasse);
        escopoClasse += 2;
        System.out.println(escopoClasse);
    }

}
